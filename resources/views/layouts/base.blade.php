<!DOCTYPE html>
<html lang="en">
  
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content>
    <meta name="author" content>
    <title>НПЦ СТ и УК</title>
    <link href="{{asset('media/bootstrap.css')}}" rel="stylesheet">
    <link href="{{asset('media/carousel.css')}}" rel="stylesheet">
	<link href="{{asset('media/css/style.css')}}" rel="stylesheet">
	@section("styles")
	@show
  </head>
  <body>

    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
      <a class="navbar-brand" href="{{asset('/')}}">
	  <img src="{{asset('media/img/logo.png')}}" width="100px">
	  </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav mr-auto">
		  <li class="nav-item dropdown show">
            <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">О компании</a>
            <div class="dropdown-menu" aria-labelledby="dropdown01">
              <a class="dropdown-item" href="{{asset('about')}}">О нас</a>
              <a class="dropdown-item" href="{{asset('galery')}}">Галерея</a>
			  <a class="dropdown-item" href="{{asset('about_work')}}">О проделанной работе</a>
			  
            </div>
          </li>
		  <li class="nav-item dropdown show">
            <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Информационный центр</a>
            <div class="dropdown-menu" aria-labelledby="dropdown01">
             
              <a class="dropdown-item" href="{{asset('journals')}}">Каталог журналов</a>
              <a class="dropdown-item" href="{{asset('new')}}">Новое в сварке</a>
			  <a class="dropdown-item" href="{{asset('tnpa')}}">ТНПА</a>
            </div>
          </li>
		  <li class="nav-item">
		   <a class="nav-link" href="{{asset('news')}}">Новости</a>
		    </li>
		<li class="nav-item">
			<a class="nav-link" href="{{asset('forum')}}">Форум</a>
			</li>
        </ul>
		<div class="nav_title text-center" >
		<a href="{{asset('/')}}" class="mylink">
		<p>Научно-производственный центр сварочных </p>
		<p>технологий и управления качеством  </p>
		<p>(НПЦ СТ и УК)</p></a>
		</div>
      </div>
    </nav>
@yield('content')

      <hr class="featurette-divider">

      <!-- /END THE FEATURETTES -->

      <!-- FOOTER -->
      <footer>
       <div class="nav_end text-center">
		<p>Юридический адрес: 220090 г. Минск, ул. Восточная, 165а, к. 302</p>
		<p>Почтовый адрес: 220012 г. Минск, ул. Чернышевского, 10а, к. 312</p>
		<p>Эл. почта: svarka77@inbox.ru</p>
		<p>Тел. номера: +8 (017) 262-65-91 </p>
		</div>
		 <p class="float-right"><a href="#">Вверх страницы</a></p>
		<p>&copy; 2018 "НПЦ СТ и УК" </p>
      </footer>

    </div><!-- /.container -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript" src="{{asset('gallery/js/jquery-2.0.3.min.js')}}"></script>
    <script src="{{asset('media/js/popper00.js')}}"></script>
    <script src="{{asset('media/js/bootstrap.js')}}"></script>
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="{{asset('media/js/holder00.js')}}"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="{{asset('media/js/ie10-vie.js')}}"></script>
	@section("scripts")
	@show
  </body>
</html>