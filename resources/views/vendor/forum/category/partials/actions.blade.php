<div class="panel panel-default" data-actions>
    <div class="panel-heading">
        <span class="glyphicon glyphicon-option-vertical"></span>
        <a href="#" data-toggle="collapse" data-target=".collapse.category-options">Действия с категориями</a>
    </div>
    <div class="collapse category-options">
        <div class="panel-body">
            <div class="form-group">
                <label for="category-action">Действия</label>
                <select name="action" id="category-action" class="form-control">
                    @can ('delete', $category)
                        <option value="delete" data-confirm="true" data-method="delete">Удалить</option>
                    @endcan

                    @can ('createCategories')
                        @if ($category->threadsEnabled)
                            <option value="disable-threads">Исключить ветку</option>
                        @else
                            <option value="enable-threads">Включить ветку</option>
                        @endif
                        @if ($category->private)
                            <option value="make-public">Сделать общедоступным</option>
                        @else
                            <option value="make-private">Сделать приватным</option>
                        @endif
                    @endcan
                    @can ('moveCategories')
                        <option value="move">Переместить</option>
                        <option value="reorder">Изменить значимость</option>
                    @endcan
                    @can ('renameCategories')
                        <option value="rename">Переименовать</option>
                    @endcan
                </select>
            </div>
            <div class="form-group hidden" data-depends="move">
                <label for="category-id">Категория</label>
                <select name="category_id" id="category-id" class="form-control">
                    <option value="0">({{ trans('forum::general.none') }})</option>
                    @include ('forum::category.partials.options', ['hide' => $category])
                </select>
            </div>
            <div class="form-group hidden" data-depends="reorder">
                <label for="new-weight">Значимость</label>
                <input type="number" name="weight" value="{{ $category->weight }}" class="form-control">
            </div>
            <div class="form-group hidden" data-depends="rename">
                <label for="new-title">Название</label>
                <input type="text" id="new-title" name="title" value="{{ $category->title }}" class="form-control">
                <label for="new-description">Описание</label>
                <input type="text" id="new-description" name="description" value="{{ $category->description }}" class="form-control">
            </div>
        </div>
        <div class="panel-footer clearfix">
            <button type="submit" class="btn btn-default pull-right">Продолжить</button>
        </div>
    </div>
</div>
