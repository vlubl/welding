{{-- $category is passed as NULL to the master layout view to prevent it from showing in the breadcrumbs --}}
@extends ('forum::master', ['category' => null])

@section ('content')
    @can ('createCategories')
        @include ('forum::category.partials.form-create')
    @endcan

    <h2>Обсуждаемые темы</h2>

    @foreach ($categories as $category)
        <table class="table table-index">
            <thead>
                <tr>
                    <th>Категория</th>
                    <th class="col-md-2">Подтемы</th>
                    <th class="col-md-2">Посты</th>
                    <th class="col-md-2">Новая ветка</th>
                    <th class="col-md-2">Последняя ветка</th>
                </tr>
            </thead>
            <tbody>
                <tr class="category">
                    @include ('forum::category.partials.list', ['titleClass' => 'lead'])
                </tr>
                @if (!$category->children->isEmpty())
                    <tr>
                        <th colspan="5">{{ trans('forum::categories.subcategories') }}</th>
                    </tr>
                    @foreach ($category->children as $subcategory)
                        @include ('forum::category.partials.list', ['category' => $subcategory])
                    @endforeach
                @endif
            </tbody>
        </table>
    @endforeach
@stop
