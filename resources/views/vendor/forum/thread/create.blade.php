@extends ('forum::master', ['breadcrumb_other' => 'Новая тема'])

@section ('content')
    <div id="create-thread">
        <h2>{{ $category->title }}</h2>

        <form method="POST" action="{{ Forum::route('thread.store', $category) }}">
            {!! csrf_field() !!}
            {!! method_field('post') !!}

            <div class="form-group">
                <label for="title">Название</label>
                <input type="text" name="title" value="{{ old('title') }}" class="form-control">
            </div>

            <div class="form-group">
                <textarea name="content" class="form-control">{{ old('content') }}</textarea>
            </div>

            <button type="submit" class="btn btn-success pull-right">Создать</button>
            <a href="{{ URL::previous() }}" class="btn btn-default">Закрыть</a>
        </form>
    </div>
@stop
