@extends('layouts.base')
@section('content')
      <div class="row featurette fon">
        <div class="col-md-12">
		  <h2 class="featurette-heading" align="center">{{$obj->name}}</h2>
			<p align="center">
			@if($obj->picture)
				<img src="{{asset('media/img/'.$obj->picture)}}" />
			@else
				 -
			@endif
			</p>
		   <p class="lead">{!!$obj->body!!}</p>
        </div>
      </div>
@endsection