@extends('layouts.base')
@section('content')
      <div class="row featurette fon">
	  <br style="clear:both" />
	  		<h2>Новости</h2>
			
        <div>
		@foreach($news as $one)
		<div class="row featurette " style="margin: 0 auto;width: 60%">
		
		  <h3 class="featurette-heading" align="center"><a href="{{asset('news/'.$one->id)}}">{{$one->name}}</a></h3>
		 
		  <div align="center"> <img class="rounded-circle"  align="left" src="{{asset('uploads/thumb/'.$one->picture)}}" alt="{{$one->name}}" width="100" height="100"><br/> {!!$one->smallbody!!}</div>
		   </div>
		@endforeach
		<p align="center">
		{!!$news->links()!!}
		</p>
        </div>
      </div>
@endsection