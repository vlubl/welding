@extends('layouts.base')
@section('styles')
@parent
    <link rel="stylesheet" type="text/css" media="all" href="{{asset('gallery/css/font-awesome.min.css')}}" />
    <link rel="stylesheet" type="text/css" media="all" href="{{asset('gallery/css/jgallery.min.css?v=1.6.0')}}" />
@endsection
@section('scripts')
@parent
   <!--<script type="text/javascript" src="js/jquery-2.0.3.min.js"></script>-->
    <script type="text/javascript" src="{{asset('gallery/js/jgallery.min.js?v=1.6.0')}}"></script>
    <script type="text/javascript" src="{{asset('gallery/js/touchswipe.min.js')}}"></script>
<script type="text/javascript">
$( function() {
    $( '#gallery' ).jGallery();
} );
</script>
@endsection
@section('content')
      <div class="row featurette fon">
        <div class="col-md-12">

<div id="gallery">
@foreach($albums as $one)
    <div class="album" data-jgallery-album-title="{{$one->name}}">
		@foreach($one->pictures as $picture)
        <a href="{{asset('uploads/'.$picture->picture)}}"><img src="{{asset('uploads/thumb/'.$picture->picture)}}" alt="{{$picture->name}}" /></a>
		@endforeach
    </div>
@endforeach
</div>
        </div>
      </div>
@endsection