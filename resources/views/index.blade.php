@extends('layouts.base')
@section('content')
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner">
        <div class="carousel-item active">
          <img class="first-slide opacity" src="media/img/1anonsmax.jpg" alt="First slide">
          <div class="container">
            <div class="carousel-caption d-none d-md-block text-left">
              <h1>14-ый конкурс сварщиков Беларуси с международным участием в рамках Белорусского промышленного форума-2018.</h1>
              <p>Время и место проведения конкурса: 29 мая - 1 июня 2018 г., г. Минск, пр-т Победителей, 20/2 (футбольный манеж).</p>
              <p><a class="btn btn-lg btn-info" href="{{asset('1anons')}}" role="button">Подробнее</a></p>
            </div>
          </div>
        </div>
        <div class="carousel-item">
          <img class="second-slide opacity" src="media/img/2anonspaton.jpg" alt="Second slide">
          <div class="container">
            <div class="carousel-caption d-none d-md-block text-left">
              <h1>Международная конференция "Сварка и родственные технологии - настоящее и будущее".</h1>
              <p>Проводит Институт сварки им. Е.О.Патона Национальной академии наук Украины 5-6 декабря 2018 года.</p>
              <p><a class="btn btn-lg btn-info" href="{{asset('2anons')}}" role="button">Подробнее</a></p>
            </div>
          </div>
        </div>
        <div class="carousel-item">
          <img class="third-slide opacity" src="media/img/3anonsrezka.jpg" alt="Third slide">
          <div class="container">
            <div class="carousel-caption d-none d-md-block text-center">
              <h1>Международная специализированная выставка «Сварка и резка-2018».</h1>
              <p>Время и место проведения: 10-13 апреля 2018 г., г. Минск, пр-т Победителей, 20/2 (футбольный манеж).</p>
              <p><a class="btn btn-lg btn-info" href="{{asset('3anons')}}" role="button">Подробнее</a></p>
            </div>
          </div>
        </div>
      </div>
      <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>


    <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

    <div class="marketing fon">
      <div class="row">
	  @foreach($news as $one)
	  <div class="col-lg-4">
          <img class="rounded-circle" src="{{asset('uploads/thumb/'.$one->picture)}}" alt="Generic placeholder image" width="100" height="100">
          <h2>{{$one->name}}</h2>
          <p>{!!$one->smallbody!!}</p>
		  <div class="date"><p>{{$one->created_at}}</p> </div>
          <p><a class="btn btn-secondary" href="{{asset('news/'.$one->id)}}" role="button">Подробнее »</a></p>
        </div>
		@endforeach
      </div>
<p align="center"><a href="{{asset('news')}}" class="btn btn-primary">Все новости</a></p>

      <!-- START THE FEATURETTES -->

      <hr class="featurette-divider">

      <div class="row featurette fon2">
        <div class="col-md-7">
          <h3 class="featurette-heading">Денисов Л.С. и Киселевич Р.А. посетили д. Грушевка. <span class="text-muted"><p>13 марта 2018 г.</p></span></h3>
          <p class="lead">Провели освидетельствование сварочного производства.</p>
        </div>
        <div class="col-md-5">
          <img class="featurette-image img-fluid mx-auto" src="{{asset('media/img/grushevo.jpg')}}" height="500" alt="Generic placeholder image">
        </div>
      </div>

      <hr class="featurette-divider">

      <div class="row featurette fon2">
        <div class="col-md-7 order-md-2">
          <h3 class="featurette-heading">Международная научно-практическая конференция. <span class="text-muted"><p>27.03.2018г.</p></span></h3>
          <p class="lead">"Эффективная сварка: качество, безопасность, ресурсосбережение"</p>
        </div>
        <div class="col-md-5 order-md-1">
          <img class="featurette-image img-fluid mx-auto" src="{{asset('media/img/konf.jpg')}}" height="500" alt="Generic placeholder image">
        </div>
      </div>

      <hr class="featurette-divider">

      <div class="row featurette fon2">
        <div class="col-md-7">
          <h2 class="featurette-heading">Качество сварки: системный подход, активный контроль, статистические методы<span class="text-muted"><p>30.03.2018г.</p></span></h2>
          <p class="lead">В конце марта в РУП "Белстройцентр" при поддержке Минстройархитектуры прошла традиционнная межотраслевая научно-практическая конференция, посвященная сварочным технологиям. Нынешняя - юбилейная - пятая по счёту. Её тема: "Эффективная сварка: качество, безопасность, ресурсосбережение", как показал весь ход мероприятия, актуальна, современна и своевременна.</p>
        </div>
        <div class="col-md-5">
          <img class="featurette-image img-fluid mx-auto" src="{{asset('media/img/effsv.jpg')}}" alt="Generic placeholder image">
        </div>
      </div>
@endsection