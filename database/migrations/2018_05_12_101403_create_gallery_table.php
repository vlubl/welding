<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;

class CreateGalleryTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Model::unguard();
        Schema::create('gallery',function(Blueprint $table){
            $table->increments("id");
            $table->string("name")->nullable();
            $table->string("picture")->nullable();
            $table->integer("album_id")->references("id")->on("album")->nullable();
            $table->string("status")->nullable();
            $table->string("sort")->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gallery');
    }

}