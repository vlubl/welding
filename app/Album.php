<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laraveldaily\Quickadmin\Observers\UserActionsObserver;


use Illuminate\Database\Eloquent\SoftDeletes;

class Album extends Model {

    use SoftDeletes;

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    protected $dates = ['deleted_at'];

    protected $table    = 'album';
    
    protected $fillable = ['name'];
    

    public static function boot()
    {
        parent::boot();

        Album::observe(new UserActionsObserver);
    }
    
        public function pictures()
    {
        return $this->hasMany('App\Gallery', 'album_id');
    }
    
    
}