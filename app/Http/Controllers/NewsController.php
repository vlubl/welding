<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;
class NewsController extends Controller
{
    public function getAll(){
		$news=News::paginate(10);
		return view('news',compact('news'));
	}
	
	public function getOne($id=Null){
		$obj=News::find($id);
		return view('news_one',compact('obj'));
	}
}
