<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Album;

class GaleryController extends Controller
{
    public function getIndex(){
		$albums = Album::all();
		return view("gallery", compact("albums"));
	}
}
