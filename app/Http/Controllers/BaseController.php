<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Maintext;
use App\News;
class BaseController extends Controller
{
    public function getIndex(){
		$news = News::orderBy('id','DESC')->limit(6)->get();
		return view("index", compact('news'));
	}
	public function getStatic($id=null){
		$obj=Maintext::where("url",$id)->first();
		return view("static",compact("obj"));
	}
}
