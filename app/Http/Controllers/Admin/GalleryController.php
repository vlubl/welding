<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Schema;
use App\Gallery;
use App\Http\Requests\CreateGalleryRequest;
use App\Http\Requests\UpdateGalleryRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Traits\FileUploadTrait;
use App\Album;


class GalleryController extends Controller {

	/**
	 * Display a listing of gallery
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index(Request $request)
    {
        $gallery = Gallery::with("album")->get();

		return view('admin.gallery.index', compact('gallery'));
	}

	/**
	 * Show the form for creating a new gallery
	 *
     * @return \Illuminate\View\View
	 */
	public function create()
	{
	    $album = Album::pluck("name", "id")->prepend('Please select', 0);

	    
	    return view('admin.gallery.create', compact("album"));
	}

	/**
	 * Store a newly created gallery in storage.
	 *
     * @param CreateGalleryRequest|Request $request
	 */
	public function store(CreateGalleryRequest $request)
	{
	    $request = $this->saveFiles($request);
		Gallery::create($request->all());

		return redirect()->route(config('quickadmin.route').'.gallery.index');
	}

	/**
	 * Show the form for editing the specified gallery.
	 *
	 * @param  int  $id
     * @return \Illuminate\View\View
	 */
	public function edit($id)
	{
		$gallery = Gallery::find($id);
	    $album = Album::pluck("name", "id")->prepend('Please select', 0);

	    
		return view('admin.gallery.edit', compact('gallery', "album"));
	}

	/**
	 * Update the specified gallery in storage.
     * @param UpdateGalleryRequest|Request $request
     *
	 * @param  int  $id
	 */
	public function update($id, UpdateGalleryRequest $request)
	{
		$gallery = Gallery::findOrFail($id);

        $request = $this->saveFiles($request);

		$gallery->update($request->all());

		return redirect()->route(config('quickadmin.route').'.gallery.index');
	}

	/**
	 * Remove the specified gallery from storage.
	 *
	 * @param  int  $id
	 */
	public function destroy($id)
	{
		Gallery::destroy($id);

		return redirect()->route(config('quickadmin.route').'.gallery.index');
	}

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        if ($request->get('toDelete') != 'mass') {
            $toDelete = json_decode($request->get('toDelete'));
            Gallery::destroy($toDelete);
        } else {
            Gallery::whereNotNull('id')->delete();
        }

        return redirect()->route(config('quickadmin.route').'.gallery.index');
    }

}
