<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laraveldaily\Quickadmin\Observers\UserActionsObserver;


use Illuminate\Database\Eloquent\SoftDeletes;

class Gallery extends Model {

    use SoftDeletes;

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    protected $dates = ['deleted_at'];

    protected $table    = 'gallery';
    
    protected $fillable = [
          'name',
          'picture',
          'album_id',
          'status',
          'sort'
    ];
    

    public static function boot()
    {
        parent::boot();

        Gallery::observe(new UserActionsObserver);
    }
    
    public function album()
    {
        return $this->hasOne('App\Album', 'id', 'album_id');
    }


    
    
    
}