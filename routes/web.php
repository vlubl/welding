<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/galery', 'GaleryController@getIndex');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('news', 'NewsController@getAll');
Route::get('news/{id}', 'NewsController@getOne');
Route::get('/', 'BaseController@getIndex');
Route::get('/{id}', 'BaseController@getStatic');// всегда будет последним




